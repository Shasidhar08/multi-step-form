let summaryTotal = 0;
let monthlyValues = [9, 12, 15];
let addonsMonthlyValues = [1, 2, 2];
let nextbtn = document.querySelector(".next-btn");
let backbtn = document.querySelector(".back-btn");
let currentMain = document.querySelector(".info-step");
let changeLink = document.querySelector(".change");
let planTypes = document.querySelector(".plan-types");
let plans = document.querySelectorAll(".plan-type");
let planTypeRadio = document.querySelectorAll(".plan-type-radio");
let costPerMonths = document.querySelectorAll(".cost-per-month");
let isMonthly = true;
let freeMonths = document.querySelectorAll(".free");
let addons = document.querySelector(".add-ons");
let toggle = document.querySelector(".toggle");
let tableData = document.querySelector(".table-data");
let controller = new AbortController();
let signal = controller.signal;
let yearlyPlans;

function togglePlan(event) {
  isMonthly = isMonthly ? false : true;
  if (isMonthly) {
    toggle.previousElementSibling.style.color = "hsl(213, 96%, 18%)";
    toggle.nextElementSibling.style.color = "hsl(229, 24%, 87%)";
    for (let i = 0; i < 3; i++) {
      freeMonths[i].classList.add("display-none");
      costPerMonths[i].innerText = `$${monthlyValues[i]}/mo`;
      addons.children[
        i
      ].lastElementChild.innerText = `$${addonsMonthlyValues[i]}/mo`;
    }
  } else {
    toggle.previousElementSibling.style.color = "hsl(229, 24%, 87%)";
    toggle.nextElementSibling.style.color = "hsl(213, 96%, 18%)";
    for (let i = 0; i < 3; i++) {
      freeMonths[i].classList.remove("display-none");
      costPerMonths[i].innerText = `$${monthlyValues[i] * 10}/mo`;
      addons.children[i].lastElementChild.innerText = `$${
        addonsMonthlyValues[i] * 10
      }/yr`;
    }
  }
}
function addDataToSummary(event, className, addOnService) {
  console.log(event.target);
  let divWrap = document.createElement("div");
  divWrap.setAttribute("class", `summary-data ${className}`);
  console.log(divWrap.classList);
  let divService = document.createElement("div");
  let divCost = document.createElement("div");
  let service = document.createElement("p");
  let cost = document.createElement("p");
  // console.log(addOnService.innerText);
  service.innerText = addOnService.innerText;
  // console.log(service.innerText);
  cost.innerText = `${event.target.nextElementSibling.nextElementSibling.innerText}`;
  divService.append(service);
  divCost.append(cost);
  divWrap.append(divService);
  divWrap.append(divCost);
  tableData.append(divWrap);
}
function removeDataFromSummary(className) {
  let element = document.querySelector(`.${className}`);
  console.log(element);
  element.remove();
}
function nextBtnDefault() {
  nextbtn.innerText = "Next Step";
  nextbtn.style.backgroundColor = "hsl(213, 96%, 18%)";
}
function backbtnHandler(event) {
  currentMain.classList.add("display-none");
  currentMain.previousElementSibling.classList.remove("display-none");
  currentMain = currentMain.previousElementSibling;
  if (currentMain.classList.contains("info-step")) {
    backbtn.classList.add("display-none");
  }
  if (currentMain.classList.contains("add-ons-step")) {
    nextBtnDefault();
  }
}

planTypes.addEventListener(
  "click",
  (event) => {
    // console.log(event.target);
    if (event.target != "plan-types") {
      planTypeRadio.forEach((value) => {
        if (value.checked) {
          // console.log(event.target);
          value.nextElementSibling.style.border =
            "1px solid hsl(243, 100%, 62%)";
          value.nextElementSibling.style.backgroundColor =
            "hsl(217, 100%, 97%)";
        } else {
          value.nextElementSibling.style.border = "";
          value.nextElementSibling.style.backgroundColor = "";
        }
      });
      // if (event.target.checked) {
      //   console.log(event);
      //   event.target.parentElement.style.border = "1px solid hsl(243, 100%, 62%)";
      //   event.target.parentElement.style.backgroundColor = "hsl(217, 100%, 97%)";
      // } else {
      // }
      // // console.log(planTypes.children);
      // // console.log(event);
    }
  },
  true
);
addons.addEventListener("click", (event) => {
  if (event.target != "add-ons") {
    let addOnService = event.target.nextElementSibling.firstElementChild;
    console.log(addOnService);
    let className = `summary-${addOnService.innerText.split(" ")[0]}`;
    if (event.target.checked) {
      console.log(event.target.parentElement);
      event.target.parentElement.style.border = "1px solid hsl(243, 100%, 62%)";
      event.target.parentElement.style.backgroundColor = "hsl(217, 100%, 97%)";

      addDataToSummary(event, className, addOnService);
    } else {
      event.target.parentElement.style.border = "";
      event.target.parentElement.style.backgroundColor = "";
      console.log(addOnService.innerText);
      removeDataFromSummary(className);
    }
  }
});

toggle.addEventListener("change", togglePlan);

changeLink.onclick = (event) => {
  event.preventDefault();
  currentMain.classList.add("display-none");
  currentMain = document.querySelector(".plan-step");
  currentMain.classList.remove("display-none");
  nextBtnDefault();
};
nextbtn.addEventListener(
  "click",
  (event) => {
    currentMain.classList.add("display-none");
    backbtn.classList.remove("display-none");
    if (currentMain.nextElementSibling)
      currentMain.nextElementSibling.classList.remove("display-none");
    currentMain = currentMain.nextElementSibling;
    if (nextbtn.innerText == "Confirm") {
      nextbtn.classList.add("display-none");
      backbtn.classList.add("display-none");
      controller.abort();
    }
    if (currentMain.classList.contains("summary-step")) {
      nextbtn.innerText = "Confirm";
      nextbtn.style.backgroundColor = "hsl(243, 100%, 62%)";
    }
  },
  { signal }
);

backbtn.addEventListener("click", backbtnHandler, { signal });
